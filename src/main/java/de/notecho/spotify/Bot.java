package de.notecho.spotify;

import de.notecho.spotify.listeners.CommandListener;
import lombok.Getter;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.sharding.DefaultShardManagerBuilder;
import net.dv8tion.jda.api.sharding.ShardManager;
import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import javax.security.auth.login.LoginException;
import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class Bot {

    private static Bot instance;

    public static Bot getInstance() {
        return instance;
    }
    @Getter
    private ShardManager shardManager;

    public static void main(String[] args) {
        try {
            instance = new Bot();
        } catch (LoginException e) {
            e.printStackTrace();
        }

    }

    public Bot() throws LoginException {
        DefaultShardManagerBuilder builder = new DefaultShardManagerBuilder();
        builder.setToken("");

        //builder.setActivity(Activity.watching("on spotys.net"));
        builder.setStatus(OnlineStatus.ONLINE);

        builder.addEventListeners(new CommandListener());

        shardManager = builder.build();
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        HttpsURLConnection connection = (HttpsURLConnection)new URL(url).openConnection();
        connection.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
        InputStream is = connection.getInputStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }

}
