package de.notecho.spotify.listeners;

import de.notecho.spotify.Bot;
import de.notecho.spotify.utils.MessageGenerator;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import java.awt.*;
import java.io.IOException;

public class CommandListener extends ListenerAdapter {

    @Override
    public void onPrivateMessageReceived(@Nonnull PrivateMessageReceivedEvent event) {
        String message = event.getMessage().getContentDisplay();
        String[] args = message.split(" ");
        if (args[0].equalsIgnoreCase("!upgrade")) {
            if (args.length == 3) {
                String key = args[1];
                String country = args[2];
                JSONObject jsonObject = null;
                try {
                    jsonObject = Bot.readJsonFromUrl("https://spoty.gg/api/v1/?action=upgrade&key=" + key + "&country=" + country);
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    return;
                }
                try {
                    if (jsonObject.getString("status").equalsIgnoreCase("success")) {
                        sendSuccessMessage(event.getAuthor(), jsonObject.getJSONObject("data").getString("adress"), jsonObject.getJSONObject("data").getString("token"));
                    } else if (jsonObject.getString("status").equalsIgnoreCase("failure")) {
                        sendFailMessage(event.getAuthor(), jsonObject.getString("message"));
                    } else {
                        sendSuccessMessage(event.getAuthor(), "null", "null");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (args[0].equalsIgnoreCase("!info")) {
            if (args.length == 2) {
                String key = args[1];
                JSONObject jsonObject = null;
                try {
                    jsonObject = Bot.readJsonFromUrl("https://spoty.gg/api/v1/?action=info&key=" + key);
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    return;
                }
                try {
                    if (jsonObject.getString("status").equalsIgnoreCase("success")) {
                        sendInfoMessage(event.getAuthor(), jsonObject.getJSONObject("data"));
                    } else if (jsonObject.getString("status").equalsIgnoreCase("failure")) {
                        sendFailMessage(event.getAuthor(), jsonObject.getString("message"));
                    } else {
                        sendSuccessMessage(event.getAuthor(), "null", "null");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onMessageReceived(@NotNull MessageReceivedEvent event) {
        String message = event.getMessage().getContentDisplay();
        if (event.isFromType(ChannelType.TEXT)) {
            TextChannel channel = event.getTextChannel();
            String[] args = message.split(" ");
            if (args[0].equalsIgnoreCase("!upgrade")) {
                if (args.length == 3) {
                    String key = args[1];
                    String country = args[2];
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = Bot.readJsonFromUrl("https://spoty.gg/api/v1/?action=upgrade&key=" + key + "&country=" + country);
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        return;
                    }
                    try {
                        if (jsonObject.getString("status").equalsIgnoreCase("success")) {
                            sendSuccessMessage(event.getAuthor(), jsonObject.getJSONObject("data").getString("adress"), jsonObject.getJSONObject("data").getString("token"));
                        } else if (jsonObject.getString("status").equalsIgnoreCase("failure")) {
                            sendFailMessage(event.getAuthor(), jsonObject.getString("message"));
                        } else {
                            sendSuccessMessage(event.getAuthor(), "null", "null");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                event.getMessage().delete().queue();
            } else if (args[0].equalsIgnoreCase("!info")) {
                if (args.length == 2) {
                    String key = args[1];
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = Bot.readJsonFromUrl("https://spoty.gg/api/v1/?action=info&key=" + key);
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        return;
                    }
                    try {
                        if (jsonObject.getString("status").equalsIgnoreCase("success")) {
                            sendInfoMessage(event.getAuthor(), jsonObject.getJSONObject("data"));
                        } else if (jsonObject.getString("status").equalsIgnoreCase("failure")) {
                            sendFailMessage(event.getAuthor(), jsonObject.getString("message"));
                        } else {
                            sendSuccessMessage(event.getAuthor(), "null", "null");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                event.getMessage().delete().queue();
            }
        }
    }

    private void sendSuccessMessage(User user, String adress, String link) {
        sendPrivateMessage(user,
                new MessageGenerator()
                        .setTitle("**SUCCESS** Key upgraded")
                        .setColor(new Color(0x0c7800))//.setFooter("Generated by Spotys.net Bot.")
                        .addField("Adress: ", "`" + adress + "`", false)
                        .addField("Invitelink: ", link, false)
                        .toMessage());
    }

    private void sendInfoMessage(User user, JSONObject data) throws JSONException {
        sendPrivateMessage(user,
                new MessageGenerator()
                        .setTitle("Keyinfo:")
                        .setColor(new Color(0x0059ff))//.setFooter("Generated by Spotys.net Bot.")
                        .addField("Adress: ", "`" + data.getString("address") + "`", false)
                        .addField("Invitelink: ", data.getString("token"), false)
                        .addField("used: ", "`" + data.getString("used") + "`", false)
                        .addField("useable: ", "`" + data.getString("useable") + "`", false)
                        .toMessage());
    }


    private void sendFailMessage(User user, String message) {
        sendPrivateMessage(user,
                new MessageGenerator()
                        .setTitle("**FAILURE**")
                        .setColor(new Color(0xff3300))//.setFooter("Generated by Spotys.net Bot.")
                        .addField("Error: ", "`" + message + "`", false)
                        .toMessage());
    }

    public void sendPrivateMessage(User user, String content) {
        user.openPrivateChannel().queue((channel) ->
        {
            channel.sendMessage(content).queue();
        });
    }

    public void sendPrivateMessage(User user, Message content) {
        user.openPrivateChannel().queue((channel) ->
        {
            channel.sendMessage(content).queue();
        });
    }
}
