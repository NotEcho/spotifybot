package de.notecho.spotify.utils;

import java.awt.Color;

import de.notecho.spotify.Bot;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;


public class MessageGenerator {
    MessageBuilder mb = new MessageBuilder();
    EmbedBuilder eb = new EmbedBuilder();


    public MessageGenerator setColor(Color color) {
        this.eb.setColor(color);
        return this;
    }


    public MessageGenerator setColor(int color) {
        this.eb.setColor(color);
        return this;
    }

    public MessageGenerator setAuthor(String author) {
        this.eb.setAuthor(author);
        return this;
    }

    public MessageGenerator setAuthor(String author, String URL) {
        this.eb.setAuthor(author, URL);
        return this;
    }

    public MessageGenerator setTitle(String title) {
        this.eb.setTitle(title);
        return this;
    }

    public MessageGenerator setTitle(String title, String URL) {
        this.eb.setTitle(title, URL);
        return this;
    }

    public MessageGenerator setThumbnail(String URL) {
        this.eb.setThumbnail(URL);
        return this;
    }

    public MessageGenerator setImage(String URL) {
        this.eb.setImage(URL);
        return this;
    }

    public MessageGenerator setFooter(String footer) {
        this.eb.setFooter(footer);
        return this;
    }

    public Message toMessage() {
        this.mb.setEmbed(this.eb.build());
        return this.mb.build();
    }

    public MessageGenerator addField(String field, String value, Boolean inline) {
        this.eb.addField(field, value, inline);
        return this;
    }
}
